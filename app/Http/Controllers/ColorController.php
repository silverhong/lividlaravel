<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;

class ColorController extends Controller
{
    public function GetData(){
        $colors = Color::All();
        return View('color',compact('colors'));
    }
    public function DeleteData($id){
        $row = Color::where('id',$id)->delete();
        return redirect('/color');
    }
    public function AddData(Request $request){
        $color = new Color;
        $color->name = $request->get('name');
        $color->save();
        return redirect('/color');
    }
    public function UpdateData(Request $request){
        $color = Color::find($request->get('id'));
        $color->name=$request->get('name');
        $color->save();
        return redirect('/color');
    }
}

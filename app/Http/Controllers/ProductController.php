<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Brand;
use App\Categories;
use App\Color;
class ProductController extends Controller
{
    //
    public function GetData()
    {
        $products = Product::All();
        $brands = Brand::All();
        $colors = Color::All();
        $categories = Categories::All();
        return View("product",compact('products','brands','colors','categories'));
    }
    public function DeleteData($id){
        $row = Product::where('id',$id)->delete();
        return redirect('/product');
    }
    public function AddData(Request $request)
    {
        $product = new Product;
        $product->name = $request->name;
        $product->brandid = $request->brand;
        $product->colorid = $request->color;
        $product->categoryid = $request->category;
        $product->instock = $request->instock;
        $product->pricein = $request->pricein;
        $product->priceout = $request->priceout;
        $product->save();
        return redirect('/product');
    }
    public function UpdateData(Request $request){
        $product = Product::find($request->get('id'));
        $product->name=$request->get('name');
        $product->brandid=$request->get('brand');
        $product->colorid=$request->get('color');
        $product->categoryid=$request->get('category');
        $product->instock=$request->get('instock');
        $product->pricein=$request->get('pricein');
        $product->priceout=$request->get('priceout');
        $product->save();
        return redirect('/product');
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Brand;

class BrandController extends Controller
{
    public function GetData(){
        $brands = Brand::All();
        return View('brand',compact('brands'));
    }
    public function AddData(Request $request){
        $brand = new Brand;
        $brand->name = $request->get('name');
        $brand->save();
        return redirect('/brand');
    }
    public function DeleteData($id){
        $row = Brand::where('id',$id)->delete();
        return redirect('/brand');
    }
    public function UpdateData(Request $request){
        $brand = Brand::find($request->get('id'));
        $brand->name=$request->get('name');
        $brand->save();
        return redirect('/brand');
    }
}

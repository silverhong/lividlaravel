<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;

class CategoriesController extends Controller
{
    public function GetData(){
        $categories = Categories::All();
        return view('categories',compact('categories'));
    }
    public function DeleteData($id){
        $row = Categories::where('id',$id)->delete();
        return redirect('/category');
    }
    public function AddData(Request $request){
        $category = new Categories;
        $category->name = $request->get('name');
        $category->save();
        return redirect('/category');
    }
    public function UpdateData(Request $request){
        $categories = Categories::find($request->get('id'));
        $categories->name=$request->get('name');
        $categories->save();
        return redirect('/category');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->nullable()->default('text');
            $table->integer('brandid')->unsigned()->nullable()->default(12);
            $table->integer('colorid')->unsigned()->nullable()->default(12);
            $table->integer('categoryid')->unsigned()->nullable()->default(12);
            $table->integer('instock')->unsigned()->nullable()->default(12);
            $table->double('pricein')->nullable();
            $table->double('priceout')->nullable();
            $table->timestamps();
            $table->foreign('brandid')->references('id')->on('brands')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('categoryid')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('colorid')->references('id')->on('colors')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

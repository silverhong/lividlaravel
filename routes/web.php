<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::View('/','main')->name('main');
Route::get('/product','ProductController@GetData')->name('product');
Route::get('/brand', 'BrandController@GetData')->name('brand');
Route::get('/delete/{id}','ProductController@DeleteData')->name('delete');
Route::get('/branddelete/{id}','BrandController@DeleteData')->name('branddelete');
Route::get('/colordelete/{id}', 'ColorController@DeleteData')->name('colordelete');
Route::get('/categorydelete/{id}','CategoriesController@DeleteData')->name('categorydelete');
Route::get('/category', 'CategoriesController@GetData')->name('category');
Route::get('/color', 'ColorController@GetData')->name('color');
Route::post('/addbrand','BrandController@AddData')->name('addbrand');
Route::post('/addcolor','ColorController@AddData')->name('addcolor');
Route::post('/addproduct','ProductController@AddData')->name('addproduct');
Route::post('/addcategory','CategoriesController@AddData')->name('addcategory');
Route::post('/updatebrand','BrandController@UpdateData')->name('updatebrand');
Route::post('/updatecategory','CategoriesController@UpdateData')->name('updatecategory');
Route::post('/updatecolor','ColorController@UpdateData')->name('updatecolor');
Route::post('/updateproduct','ProductController@UpdateData')->name('updateproduct');


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../files/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../files/img/newlogo.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Livid 
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <!-- CSS Files -->
  <link href="../files/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../files/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../files/demo/demo.css" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="../files/img/newlogo.png">
          </div>
        </a>
        <a href="" class="simple-text logo-normal">
          Livid 
          <!-- <div class="logo-image-big">
            <img src="../files/img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="{{ request()->is('/') ? 'active' : '' }}">
            <a href="{{Route('main')}}">
              <i class="nc-icon nc-bank"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="{{ request()->is('product*') ? 'active' : '' }}">
            <a href="{{Route('product')}}">
              <i class="nc-icon nc-tile-56"></i>
              <p>Inventory</p>
            </a>
          </li>
          <li class="{{ request()->is('brand*') ? 'active' : '' }}">
            <a href="{{Route('brand')}}">
              <i class="nc-icon nc-tile-56"></i>
              <p>Brand</p>
            </a>
          </li>
          <li class="{{ request()->is('category*') ? 'active' : '' }}">
            <a href="{{Route('category')}}">
              <i class="nc-icon nc-tile-56"></i>
              <p>Category</p>
            </a>
          </li>
          <li class="{{ request()->is('color*') ? 'active' : '' }}">
            <a href="{{Route('color')}}">
              <i class="nc-icon nc-tile-56"></i>
              <p>Color</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">

      @yield('body')
  
  <canvas id="bigDashboardChart"></canvas>
  <!--   Core JS Files   -->
  <script src="../files/js/core/jquery.min.js"></script>
  <script src="../files/js/core/popper.min.js"></script>
  <script src="../files/js/core/bootstrap.min.js"></script>
  <script src="../files/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="../files/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../files/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../files/js/paper-dashboard.min.js?v=2.0.0" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="../files/demo/demo.js"></script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in files/files-for-demo/js/demo.js
      demo.initChartsPages();
    });
  </script>
</body>

</html>
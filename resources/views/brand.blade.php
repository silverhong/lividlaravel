@extends('main')
@section('body')
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title"> Brand</h4>
        </div>
        <div class="card-body">
          <!-- A nis modal -->
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Brand</button>
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="{{url('/addbrand')}}" method="POST">
                  @method('POST')
                  @csrf
                  <div class="form-group">
                    <div class="container"><label for="name"> Brand Name : </label>
                      <input type="text" class="form-control" name="name">
                      <div class="form-group d-flex align-items-center justify-content-between">
                        <button type="submit" class="btn btn-primary form-control mr-2">Submit</button>
                        <button type="button" class="btn btn-danger form-control mr-2" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <!-- Modal job trim ng -->
        </div>
        <div class="table">
          <table class="table">
            <thead class=" text-primary">
              <th>
                No
              </th>
              <th>
                Names
              </th>
              <th>
                Action
              </th>
            </thead>
            <tbody>
              @foreach($brands as $brand)
              <tr>
                <td>{{$brand->id}}</td>
                <td>{{$brand->name}}</td>
                <td>
                  <span>
                    <!-- A nis modal -->
                    <a href="#" data-toggle="modal" data-target="#updateBrand{{$brand->id}}"><img src="../files/img/edit_26px.png"></a>
                    <div class="modal fade" id="updateBrand{{$brand->id}}" role="dialog">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <form action="{{url('/updatebrand')}}" method="POST">
                            @csrf
                            <div class="container">
                              <h6>Update Brand</h6>
                              <div class="form-group">
                                <!-- <label for="id">ID : </label> -->
                                <input type="hidden" class="form-control" name="id" value="{{$brand->id}}" readonly>
                              </div>
                              <div class="form-group">
                                <label for="name">Brand Name : </label>
                                <input type="text" class="form-control" name="name" value="{{$brand->name}}" autocomplete="off">
                              </div>
                              <div class="form-group d-flex align-items-center justify-content-between">
                                <button type="submit" class="btn btn-primary form-control mr-2">Update</button>
                                <button type="button" class="btn btn-danger form-control mr-2" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>
                        </div>
                        </form>
                      </div>
                    </div>
        </div>
        <!-- Modal job trim ng -->
        | <a href="{{Route('branddelete',$brand->id)}}"><img src="../files/img/trash_26px.png"></a>
        </span>
        </td>
        </tr>
        @endforeach
        </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@extends('main')
@section('body')
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title"> Car in stock</h4>
        </div>
        <div class="card-body">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Product</button>
          <!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="{{url('/addproduct')}}" method="POST">
                  @method('POST')
                  @csrf
                  <div class="form-group">
                    <div class="container">
                      <label for="name"> Name : </label>
                      <input type="text" class="form-control" name="name" required>
                      <label for="brand"> Brand : </label>
                      <select name="brand" class="custom-select" required>
                        @foreach($brands as $brand)
                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                        @endforeach
                      </select>
                      <label for="color"> Color : </label>
                      <select name="color" class="custom-select" required>
                        @foreach($colors as $color)
                        <option value="{{$color->id}}">{{$color->name}}</option>
                        @endforeach
                      </select>
                      <label for="category"> Category : </label>
                      <select name="category" class="custom-select" required>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                      </select>
                      <label for="instock"> Instock : </label>
                      <input type="text" class="form-control" name="instock" required>
                      <label for="pricein"> Price In : </label>
                      <input type="text" class="form-control" name="pricein" required>
                      <label for="priceout"> Price Out : </label>
                      <input type="text" class="form-control" name="priceout" required>
                      <div class="form-group d-flex align-items-center justify-content-between">
                        <button type="submit" class="btn btn-primary form-control mr-2">Submit</button>
                        <button type="button" class="btn btn-danger form-control mr-2" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="table">
            <table class="table">
              <thead class=" text-primary">
                <th>
                  No
                </th>
                <th>
                  Names
                </th>
                <th>
                  Brand
                </th>
                <th>
                  Color
                </th>
                <th>
                  Category
                </th>
                <th>
                  Instock
                </th>
                <th>
                  Price Out
                </th>

                <th>
                  Option
                </th>
              </thead>
              <tbody>
                @foreach($products as $product)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$product->name}}</td>
                  @foreach($brands as $brand)
                  @if ($brand->id==$product->brandid)
                  <td>{{$brand->name}}</td>
                  @break
                  @endif
                  @endforeach
                  @foreach($colors as $color)
                  @if ($color->id==$product->colorid)
                  <td>{{$color->name}}</td>
                  @break
                  @endif
                  @endforeach
                  @foreach($categories as $category)
                  @if ($category->id==$product->categoryid)
                  <td>{{$category->name}}</td>
                  @break
                  @endif
                  @endforeach
                  <td>{{$product->instock}}</td>
                  <td>$ {{$product->priceout}}</td>
                  <td>
                    <span>
                      <a href="#" data-toggle="modal" data-target="#updateBrand{{$product->id}}"><img src="../files/img/edit_26px.png"></a>
                      <div class="modal fade" id="updateBrand{{$product->id}}" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form action="{{url('/updateproduct')}}" method="POST">
                              @csrf
                              <div class="container">
                                <h6>Update Product</h6>
                                <div class="form-group">
                                  <!-- <label for="id">ID : </label> -->
                                  <input type="hidden" class="form-control" name="id" value="{{$product->id}}" readonly>
                                </div>
                                <div class="form-group">
                                  <label for="name">Name : </label>
                                  <input type="text" class="form-control" name="name" value="{{$product->name}}" required>
                                </div>
                                <div class="form-group">
                                  <label for="brand">Brand : </label>
                                  <select name="brand" class="custom-select" required>
                                    @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="color"> Color : </label>
                                  <select name="color" class="custom-select" required>
                                    @foreach($colors as $color)
                                    <option value="{{$color->id}}">{{$color->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="category"> Category : </label>
                                  <select name="category" class="custom-select" required>
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="instock">In Stock : </label>
                                  <input type="text" class="form-control" name="instock" value="{{$product->instock}}" required>
                                </div>
                                <div class="form-group">
                                  <label for="pricein">Price In : </label>
                                  <input type="text" class="form-control" name="pricein" value="{{$product->pricein}}" required>
                                </div>
                                <div class="form-group">
                                  <label for="priceout">Price Out : </label>
                                  <input type="text" class="form-control" name="priceout" value="{{$product->priceout}}" required>
                                </div>
                                <div class="form-group d-flex align-items-center justify-content-between">
                                  <button type="submit" class="btn btn-primary form-control mr-2">Update</button>
                                  <button type="button" class="btn btn-danger form-control mr-2" data-dismiss="modal">Cancel</button>
                                </div>
                              </div>
                          </div>
                          </form>
                        </div>
                      </div>
          </div>
           |  <a href="{{Route('delete',$product->id)}}"><img src="../files/img/trash_26px.png"></a></span></td>
          </tr>
          @endforeach
          </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
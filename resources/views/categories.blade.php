@extends('main')
@section('body')
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title"> Category</h4>
        </div>
        <div class="card-body">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Category</button>
          <!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="{{url('/addcategory')}}" method="POST">
                  @method('POST')
                  @csrf
                  <div class="form-group">
                    <div class="container"><label for="name"> Category Name : </label>
                      <input type="text" class="form-control" name="name">
                      <div class="form-group d-flex align-items-center justify-content-between">
                                  <button type="submit" class="btn btn-primary form-control mr-2">Submit</button>
                                  <button type="button" class="btn btn-danger form-control mr-2" data-dismiss="modal">Cancel</button>
                                </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="table">
            <table class="table">
              <thead class=" text-primary">
                <th>
                  No
                </th>
                <th>
                  Names
                </th>
                <th>
                  Action
                </th>
              </thead>
              <tbody>
                @foreach($categories as $category)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$category->name}}</td>
                  <td>
                    <span>
                      <a href="#" data-toggle="modal" data-target="#updateBrand{{$category->id}}" name="update[]"><img src="../files/img/edit_26px.png"></a>
                      <div class="modal fade" id="updateBrand{{$category->id}}" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form action="{{url('/updatecategory')}}" method="POST">
                              @method("POST")
                              @csrf
                              <div class="container">
                                <h6>Update Category</h6>
                                <div class="form-group">
                                  <!-- <label for="id">ID : </label> -->
                                  <input type="hidden" class="form-control" name="id" value="{{$category->id}}" readonly>
                                </div>
                                <div class="form-group">
                                  <label for="name">Category Name : </label>
                                  <input type="text" class="form-control" name="name" value="{{$category->name}}" autocomplete="off">
                                </div>
                                <div class="form-group d-flex align-items-center justify-content-between">
                                  <button type="submit" class="btn btn-primary form-control mr-2">Update</button>
                                  <button type="button" class="btn btn-danger form-control mr-2" data-dismiss="modal">Cancel</button>
                                </div>
                              </div>
                          </div>
                          </form>
                        </div>
                      </div>
                      | <a href="{{Route('categorydelete',$category->id)}}"><img src="../files/img/trash_26px.png"></a>
                    </span>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection